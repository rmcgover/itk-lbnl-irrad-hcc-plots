import argparse
import csv
import ctypes
import datetime as dt
import enum
import glob
import numpy as np
import os.path
import pickle

import ROOT
import AtlasStyle
AtlasStyle.SetAtlasStyle()
ROOT.gStyle.SetPalette(ROOT.kThermometer)

import matplotlib.animation as anm
import matplotlib.colors as mpc
import matplotlib.pyplot as plt
from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})


# Get from here: https://docs.google.com/spreadsheets/d/1bDEX789o99ZlcBs_pm-LhJ-2avx183WI8BkwaBDYKSE/edit?usp=sharing
STRPTIME_FMT = '%Y-%m-%d %H:%M'
IRRAD_RUN_DATA = {
  'HCC504': {
    'start': dt.datetime.strptime('2022-06-29 10:28', STRPTIME_FMT),
    'duration': dt.timedelta(hours=6, minutes=15),
    'doserate': 0.8, # MRad/h
  },
  'HCC505': {
    'start': dt.datetime.strptime('2022-06-29 18:05', STRPTIME_FMT),
    'duration': dt.timedelta(hours=6, minutes=15),
    'doserate': 0.8, # MRad/h
  },
  'HCC506': {
    'start': dt.datetime.strptime('2022-06-30 13:09', STRPTIME_FMT),
    'duration': dt.timedelta(hours=6, minutes=15),
    'doserate': 0.8, # MRad/h
  },
  'HCC507': {
    'start': dt.datetime.strptime('2022-07-01 07:49', STRPTIME_FMT),
    'duration': dt.timedelta(hours=6, minutes=15),
    'doserate': 0.8, # MRad/h
  },
  'HCC508': {
    'start': dt.datetime.strptime('2022-07-01 16:24', STRPTIME_FMT),
    'duration': dt.timedelta(hours=6, minutes=15),
    'doserate': 0.8, # MRad/h
  },
  'HCC510': {
    'start': dt.datetime.strptime('2022-07-05 08:28', STRPTIME_FMT),
    'duration': dt.timedelta(hours=6, minutes=15),
    'doserate': 0.8, # MRad/h
  },
  'HCC511': {
    'start': dt.datetime.strptime('2022-07-05 15:48', STRPTIME_FMT),
    'duration': dt.timedelta(hours=6, minutes=15),
    'doserate': 0.8, # MRad/h
  },
  'HCC512': {
    'start': dt.datetime.strptime('2022-07-06 09:48', STRPTIME_FMT),
    'duration': dt.timedelta(hours=6, minutes=15),
    'doserate': 0.8, # MRad/h
  },
  'HCC510TID': {
    'start': dt.datetime.strptime('2022-07-14 09:40', STRPTIME_FMT),
    'duration': dt.timedelta(hours=31, minutes=47),
    'doserate': 2.36, # MRad/h
    'start_dose': 5.00 # MRad
  },
  'HCC511TID': {
    'start': dt.datetime.strptime('2022-07-18 09:14', STRPTIME_FMT),
    'duration': dt.timedelta(hours=31, minutes=47),
    'doserate': 2.36, # MRad/h
    'start_dose': 5.00
  },
  'HCC512TID': {
    'start': dt.datetime.strptime('2022-07-19 17:14', STRPTIME_FMT),
    'duration': dt.timedelta(hours=31, minutes=47),
    'doserate': 2.36, # MRad/h
    'start_dose': 5.00
  },
}


class tc:
    RESET   = '\033[0m'
    BLACK   = '\033[30m'
    RED     = '\033[31m'
    GREEN   = '\033[32m'
    BLUE    = '\033[34m'
    MAGENTA = '\033[35m'


def file_sort_key(file_path):
  '''Break root file names into the useful pieces'''
  file_name = os.path.splitext(os.path.basename(file_path))[0]
  name, timestamp = file_name.split('_')
  name_split = name.split('-')
  run_name = '-'.join(name_split[:-3])
  run_number = int(name_split[-3][3:])
  loop = int(name_split[-2])
  irrad_probe = name_split[-1]
  return (run_name, run_number, loop, irrad_probe, timestamp)


CORRUPT_FILES = (
  'HCC504-Run1-11-Irrad_220629134619.root',
  'HCC511-Run1-49-Irrad_220706084155.root',
)


def file_is_corrupt(file_path):
  '''Just skip the file if it's in the list of known-corrupt files'''
  file_name = os.path.basename(file_path)
  return file_name in CORRUPT_FILES


def main(args):
  '''Driver function: sort input files into runs and make plots'''
  # Get probe and irrad file paths and sort into runs
  file_paths = glob.glob(os.path.join(args.in_dir, 'HCC*-Run*.root'))
  file_paths = sorted(file_paths, key=file_sort_key)
  file_path_dict = {}
  for file_path in file_paths:
    run_name, run_number, loop, irrad_probe, _ = file_sort_key(file_path)
    if 'Anneal' in run_name: continue # This data ain't useful
    if file_is_corrupt(file_path): continue
    if run_name not in file_path_dict:
      file_path_dict[run_name] = {}
    loop_idx = (run_number, loop)
    if loop_idx not in file_path_dict[run_name]:
      file_path_dict[run_name][loop_idx] = {}
    file_path_dict[run_name][loop_idx][irrad_probe] = file_path

  # Load cached data from disk
  cache_path = os.path.join(args.out_dir, 'run_dict.pkl')
  if args.purge or not os.path.exists(cache_path):
    run_dict = {}
  else:
    with open(cache_path, 'rb') as pkl:
      run_dict = pickle.load(pkl)
  # Make plots of individual irradiation runs
  for run_name, run_path_dict in file_path_dict.items():
    # Get data for plotting
    if run_name not in run_dict:
      run_dict[run_name] = {}
      # Gather probe and irrad files
      loop_idxs = sorted(run_path_dict)
      irrad_file_paths = []
      probe_file_paths = []
      probe_file_timestamps = []
      for loop_idx in loop_idxs:
        if 'Irrad' not in run_path_dict[loop_idx]:
          continue
        irrad_file_path = run_path_dict[loop_idx]['Irrad']
        irrad_file_paths.append(irrad_file_path)
        if 'Probe' not in run_path_dict[loop_idx]:
          continue
        loop_start = dt.datetime.strptime(file_sort_key(irrad_file_path)[-1], '%y%m%d%H%M%S')
        probe_file_paths.append(run_path_dict[loop_idx]['Probe'])
        irrad_file = ROOT.TFile.Open(irrad_file_path, "READ")
        DCS_tree = irrad_file.Get("DCS")
        df = ROOT.RDataFrame(DCS_tree)
        max_ts = df.Max('Timestamp').GetValue()
        irrad_loop_end = loop_start + dt.timedelta(milliseconds=max_ts)
        probe_file_timestamps.append(irrad_loop_end)
      # Get data for plotting
      run_start = dt.datetime.strptime(file_sort_key(irrad_file_paths[0])[-1], '%y%m%d%H%M%S')
      irrad_data = process_DCS_Irrad(irrad_file_paths)
      probe_data, probe_data_min, probe_data_max = \
        process_LDOLoop_Probe(probe_file_paths, probe_file_timestamps, run_start) # DEBUG
      # Cache data for next plot loop
      run_dict[run_name]['run_start'] = run_start
      run_dict[run_name]['irrad_data'] = irrad_data
      run_dict[run_name]['probe_file_paths'] = probe_file_paths
      run_dict[run_name]['probe_file_timestamps'] = probe_file_timestamps
      run_dict[run_name]['probe_data'] = probe_data
      run_dict[run_name]['probe_data_min'] = probe_data_min
      run_dict[run_name]['probe_data_max'] = probe_data_max
    else:
      # Load cached run data
      run_start = run_dict[run_name]['run_start']
      irrad_data = run_dict[run_name]['irrad_data']
      probe_file_paths = run_dict[run_name]['probe_file_paths']
      probe_file_timestamps = run_dict[run_name]['probe_file_timestamps']
      probe_data = run_dict[run_name]['probe_data']
      probe_data_min = run_dict[run_name]['probe_data_min']
      probe_data_max = run_dict[run_name]['probe_data_max']
    # Make plots
    print(f'Making plots for run "{run_name}"')
    # plot_DCS_Irrad(args, run_name, run_start, irrad_data)
    # plot_LDOLoop_Probe(args, run_name, run_start, probe_data, probe_data_min, probe_data_max)
    # plot_Shmoo_Probe(args, probe_file_paths, probe_file_timestamps, run_start)

  # Serialize data to disk
  with open(cache_path, 'wb') as pkl:
    pickle.dump(run_dict, pkl, pickle.HIGHEST_PROTOCOL)

  exit()

  # Make fit plots
  irrad_tid_rows = []
  irrad_anneal_rows = []
  probe_tid_rows = []
  for run_name in run_dict:
    run_start = run_dict[run_name]['run_start']
    irrad_data = run_dict[run_name]['irrad_data']
    probe_data = run_dict[run_name]['probe_data']
    probe_data_min = run_dict[run_name]['probe_data_min']
    probe_data_max = run_dict[run_name]['probe_data_max']
    # Irrad TID bump fit
    result = plot_DCS_IrradTIDFit(args, run_name, irrad_data)
    if result is not None:
      header, row = result
      if not irrad_tid_rows:
        irrad_tid_rows.append(header)
      irrad_tid_rows.append(row)
    # Irrad Annealing fit
    result = plot_DCS_IrradAnnealFit(args, run_name, run_start, irrad_data)
    if result is not None:
      header, row = result
      if not irrad_anneal_rows:
        irrad_anneal_rows.append(header)
      irrad_anneal_rows.append(row)
    # Probe TID bump fit
    result = plot_LDOLoop_ProbeTIDFit(args, run_name, probe_data)
    if result is not None:
      header, rows = result
      if not probe_tid_rows:
        probe_tid_rows.append(header)
      probe_tid_rows.extend(rows)

  # Write out fit data to csv
  if irrad_tid_rows:
    fit_csv_path = os.path.join(args.out_dir, 'Fits', 'IrradTIDFit.csv')
    write_fit_csv(fit_csv_path, irrad_tid_rows)
  if irrad_anneal_rows:
    fit_csv_path = os.path.join(args.out_dir, 'Fits', 'IrradAnnealFit.csv')
    write_fit_csv(fit_csv_path, irrad_anneal_rows)
  if probe_tid_rows:
    fit_csv_path = os.path.join(args.out_dir, 'Fits', 'ProbeTIDFit.csv')
    write_fit_csv(fit_csv_path, probe_tid_rows)

  # Make plots of combined/multiple irradiation runs
  # plot_DCS_IrradDoses(args, run_dict)


# Configure x-axis title
def get_axis_title(var):
  '''Get axis title for a given HCC TTree variable'''
  var_name = var.replace('_', ' ')
  if 'IDD' in var:
    return var_name + ' [mA]'
  elif 'Dose' in var:
    return var_name + ' [MRad]'
  elif 'NTC0' in var:
    return var_name + ' [C]'
  elif var.startswith('AM_'):
    if 'gain' in var:
      return var_name
    elif var.endswith('_calc'):
      return var_name + ' [V]'
    else:
      return var_name + ' [counts]'
  elif 'Power' in var:
    return var_name + ' [mW]'
  else:
    return var_name + ' [V]'


def draw_irrad_startstop(canvas, run_start, irrad_start, irrad_duration):
  '''Draw lines corresponding to the start and end of beam'''
  canvas.Update()
  frame = canvas.GetFrame()
  x1, x2, y1, y2 = frame.GetX1(), frame.GetX2(), frame.GetY1(), frame.GetY2()
  # x1, x2, y1, y2 = canvas.GetUxmin(), canvas.GetUxmax(), canvas.GetUymin(), canvas.GetUymax()
  # x1, x2, y1, y2 = ROOT.gPad.GetUxmin(), ROOT.gPad.GetUxmax(), ROOT.gPad.GetUymin(), ROOT.gPad.GetUymax()
  start_s = (irrad_start - run_start) / dt.timedelta(seconds=1)
  stop_s = ((irrad_start - run_start) + irrad_duration) / dt.timedelta(seconds=1)
  line_start = ROOT.TLine(start_s, y1, start_s, y2)
  line_stop = ROOT.TLine(stop_s, y1, stop_s, y2)
  line_start.SetLineColor(ROOT.kRed)
  line_stop.SetLineColor(ROOT.kRed)
  line_start.DrawClone()
  line_stop.DrawClone()


def process_DCS_Irrad(irrad_file_paths):
  '''Gather DCS data from the irradiation loops for a single run'''
  run_name = file_sort_key(irrad_file_paths[0])[0]

  # Run data for analysis
  irrad_run_data = IRRAD_RUN_DATA.get(run_name, None)
  beam_start = irrad_run_data['start']
  beam_duration = irrad_run_data['duration']
  start_dose = irrad_run_data.get('start_dose', 0.)
  doserate = irrad_run_data['doserate']
  run_dose = doserate * (beam_duration / dt.timedelta(hours=1))

  # Variables to get from TTree
  vars = (
    'Timestamp', #*
    'FMC_bandgap',
    'FMC_VDDraw', #*
    'FMC_VDDreg', #*
    'FMC_IDD', #*
    'FMC_VDD',
    'FMC_NTC0',
    'AM_bandgap',
    'AM_temp',
    'AM_VDDreg',
    'AM_VDDraw',
    'AM_HybridGRD',
    'AM_Cal',
    'AM_bandgap_calc',
    'AM_temp_calc',
    'AM_VDDreg_calc',
    'AM_VDDraw_calc',
    'AM_HybridGRD_calc',
    'AM_Cal_calc',
    'AM_gain',
    'CAL_slope_set',
    'CAL_offset_set',
    'Dose', #*
  )
  calculated_vars = ('Dose', 'BeamOn',)
  vars += calculated_vars

  # Get data from DCS trees (probably should use dataframe instead...)
  data = {var: [] for var in vars}
  run_start = dt.datetime.strptime(file_sort_key(irrad_file_paths[0])[-1], '%y%m%d%H%M%S')
  for file_path in irrad_file_paths:
    loop_start = dt.datetime.strptime(file_sort_key(file_path)[-1], '%y%m%d%H%M%S')
    file = ROOT.TFile.Open(file_path, "READ")
    DCS_tree = file.Get("DCS")
    for entry in DCS_tree:
      # Skip first entry
      if getattr(entry, 'AM_VDDraw') < 1:
        continue
      for var in data:
        if var in calculated_vars: continue
        val = getattr(entry, var)
        if var == 'Timestamp':
          # Timestamp is in milliseconds since start of loop
          # Need to add offset from start of run
          # Then, for ROOT we need to convert to seconds
          val = (dt.timedelta(milliseconds=val) + (loop_start - run_start)) / dt.timedelta(seconds=1)
          # Calculate dose as well
          time_in_beam = (run_start + dt.timedelta(seconds=val)) - beam_start
          dose = start_dose
          beam_on = False
          if time_in_beam > dt.timedelta(0):
            if time_in_beam > beam_duration:
              dose += run_dose
            else:
              dose += doserate * (time_in_beam / dt.timedelta(hours=1))
              beam_on = True
          data['Dose'].append(dose)
          data['BeamOn'].append(beam_on)
        data[var].append(val)

  return data

def plot_DCS_Irrad(args, run_name, run_start, data):
  '''Plot DCS data from the irradiation loops for a single run'''
  # Variables to plot
  vars = (
    'Timestamp', #*
    'FMC_bandgap',
    'FMC_VDDraw', #*
    'FMC_VDDreg', #*
    'FMC_IDD', #*
    'FMC_VDD',
    'FMC_NTC0',
    'AM_bandgap',
    'AM_temp',
    'AM_VDDreg',
    'AM_VDDraw',
    'AM_HybridGRD',
    'AM_Cal',
    'AM_bandgap_calc',
    'AM_temp_calc',
    'AM_VDDreg_calc',
    'AM_VDDraw_calc',
    'AM_HybridGRD_calc',
    'AM_Cal_calc',
    'AM_gain',
    'CAL_slope_set',
    'CAL_offset_set',
    'Dose', #*
  )


  # Run data for analysis
  irrad_run_data = IRRAD_RUN_DATA.get(run_name, None)
  beam_start = irrad_run_data['start']
  beam_duration = irrad_run_data['duration']
  # start_dose = irrad_run_data.get('start_dose', 0.)
  # doserate = irrad_run_data['doserate']
  # run_dose = doserate * (beam_duration / dt.timedelta(hours=1))

  # Make plot (sub)directory
  plot_dir = os.path.join(args.out_dir, run_name)
  os.makedirs(plot_dir, exist_ok=True)
  if args.pdf:
    pdf_subdir = os.path.join(plot_dir, 'pdf')
    os.makedirs(pdf_subdir, exist_ok=True)

  # Plot variables as a function of time
  c1 = ROOT.TCanvas('HCC', 'HCC', 600, 400)
  c1.cd()
  for var in vars:
    if var == 'Timestamp' or var == 'BeamOn':
      continue
    this_graph = ROOT.TGraph()
    this_graph.SetTitle(run_name)
    # Massage data and fill TGraphs
    for i, (ts, val) in enumerate(zip(data['Timestamp'], data[var])):
      if var.startswith("AM_"):
        # Skip bad AM reads
        if var.endswith("_calc") and val < -9998:
          continue
        elif val == -1:
          continue
      if var.startswith("CAL_") and val < -9998:
        # Skip bad CAL reads
        continue
      this_graph.SetPoint(this_graph.GetN(), ts, val)
    # Auto-find y-axis range
    max_yvalue = max(this_graph.GetY())
    min_yvalue = min(this_graph.GetY())
    diff = max_yvalue - min_yvalue
    this_graph.SetMaximum(max_yvalue + diff*0.6)
    this_graph.SetMinimum(min_yvalue - diff*0.2)
    # Draw plot
    this_graph.GetXaxis().SetTimeDisplay(1)
    this_graph.GetXaxis().SetTimeOffset(run_start.timestamp())
    this_graph.GetXaxis().SetLabelSize(0.038) # Default 0.04
    if irrad_run_data is not None:
      x_label = f'Time (started {beam_start.strftime(STRPTIME_FMT)})'
    else:
      x_label = 'Time'
    this_graph.GetXaxis().SetTitle(x_label)
    this_graph.GetYaxis().SetTitle(get_axis_title(var))
    this_graph.SetMarkerSize(0.4)
    this_graph.Draw("A P")
    if irrad_run_data is not None:
      draw_irrad_startstop(c1, run_start, beam_start, beam_duration)
    plot_title = f'Irradiation Run #font[82]{{{run_name}}}'
    AtlasStyle.ATLAS_LABEL(0.2, 0.88, 1, f'Internal')
    AtlasStyle.myText(0.2, 0.84, 1, plot_title)
    # Save plot
    plot_name = f'{run_name}-Irrad-{var}'
    c1.SaveAs(os.path.join(plot_dir, f'{plot_name}.png'))
    if args.pdf:
      c1.SaveAs(os.path.join(pdf_subdir, f'{plot_name}.pdf'))
    c1.Clear()

  return data


def write_fit_csv(file_path, rows):
  '''Write csv file'''
  with open(file_path, 'w', newline='') as f:
    writer = csv.writer(f)
    writer.writerows(rows)

def get_fit_csv_header(fit):
  '''Get csv header from ROOT TF1 fit'''
  row = ['fit_name']
  n_params = fit.GetNumberFreeParameters()
  for i in range(n_params):
    var = fit.GetParName(i)
    row += [
      var, f'{var}_err', f'{var}_fit_min', f'{var}_fit_max'
    ]
  row += [
    'chi2/ndof'
  ]
  return row


def get_fit_csv_row(fit, fit_name):
  '''Get csv row from ROOT TF1 fit'''
  row = [fit_name]
  n_params = fit.GetNumberFreeParameters()
  for i in range(n_params):
    val = fit.GetParameter(i)
    err = fit.GetParError(i)
    var_fit_min, var_fit_max = ctypes.c_double(), ctypes.c_double()
    fit.GetParLimits(i, var_fit_min, var_fit_max)
    row += [
      val, err, var_fit_min.value, var_fit_max.value
    ]
  chi2_div_ndof = fit.GetChisquare() / fit.GetNDF()
  row += [chi2_div_ndof]
  return row


# Note to future Bobby: the peak is generally located here,
# but there's another transcendental equation given by
# C1 e^(-x/C2) - C3 e^(-x/C4) = C1 - C3 - C5
def get_TID_peak_loc(fit, result):
  '''Return the dose at the TID peak and its error'''
  # NB peak location is independent of C5
  C1, C2, C3, C4 = (fit.GetParameter(i) for i in range(4))
  num = np.log((C1 / C3) / (C2 / C4))
  den = (1 / C2 - 1 / C4)
  x = num / den
  # σi = np.array([fit.GetParError(i) for i in range(4)])
  Σ = np.array([[result.CovMatrix(i, j) for i in range(4)] for j in range(4)])
  dxdC1 = 1 / (C1 * den)
  dxdC2 = (-1 + x / C2) / (C2 * den)
  dxdC3 = -1 / (C3 * den)
  dxdC4 = (1 - x / C4) / (C4 * den)
  J = np.array([dxdC1, dxdC2, dxdC3, dxdC4])
  # σx = np.sqrt(np.sum(σi**2 * J**2)) # Much larger error!
  σx = np.sqrt(J @ Σ @ J)
  return x, σx


# def get_TID_peak_loc(fit, result):
#   '''Return the dose at the TID peak and its error'''
#   # NB peak location is independent of C1 and C5
#   C2, C3, C4 = (fit.GetParameter(i) for i in range(1, 4))
#   num = np.log((1 / C3) / (C2 / C4))
#   den = (1 / C2 - 1 / C4)
#   x = num / den
#   # σi = np.array([fit.GetParError(i) for i in range(4)])
#   Σ = np.array([[result.CovMatrix(i, j) for i in range(1, 4)] for j in range(1, 4)])
#   # dxdC1 = 1 / (C1 * den)
#   dxdC2 = (-1 + x / C2) / (C2 * den)
#   dxdC3 = -1 / (C3 * den)
#   dxdC4 = (1 - x / C4) / (C4 * den)
#   J = np.array([dxdC2, dxdC3, dxdC4])
#   # σx = np.sqrt(np.sum(σi**2 * J**2)) # Much larger error!
#   σx = np.sqrt(J @ Σ @ J)
#   return x, σx


def plot_DCS_IrradTIDFit(args, run_name, data):
  '''Fit TID bumps from the irradiation loop DCS data'''
  # For TID fits: censor 5-8 MRad for continuity?
  # TODO: skip for now
  if 'TID' in run_name:
    return

  # Variable to plot
  var = 'FMC_IDD'

  # Range for fitting
  fit_xrange = (0.2, 5.) # MRad

  # Make plot (sub)directory
  plot_dir = os.path.join(args.out_dir, 'Fits')
  os.makedirs(plot_dir, exist_ok=True)
  if args.pdf:
    pdf_subdir = os.path.join(plot_dir, 'pdf')
    os.makedirs(pdf_subdir, exist_ok=True)

  # Plot data during the beam
  c1 = ROOT.TCanvas('HCC', 'HCC', 600, 400)
  c1.cd()
  this_graph = ROOT.TGraph()
  this_graph.SetTitle(run_name)
  this_y0 = None
  for dose, beamon, val in zip(data['Dose'], data['BeamOn'], data[var]):
    if not beamon:
      continue
    if this_y0 is None and dose >= fit_xrange[0]:
      this_y0 = val
    this_graph.SetPoint(this_graph.GetN(), dose, val)
  # Auto-find y-axis range
  max_yvalue = max(this_graph.GetY())
  min_yvalue = min(this_graph.GetY())
  diff = max_yvalue - min_yvalue
  this_graph.SetMaximum(max_yvalue + diff*0.6)
  this_graph.SetMinimum(min_yvalue - diff*0.2)
  # Fit TID bump
  # functional form from slide 8 of https://indico.cern.ch/event/728934/#132-slow-tid-irradiations
  this_fit = ROOT.TF1(
    "Fit_TIDbump",
    "([0]*(1-e^(-x/[1]))-[2]*(1-e^(-x/[3]))-[4])^2+"+str(this_y0),
    # "([0]*(1-e^(-x/[1]))-[2]*(1-e^(-x/[3])))^2+"+str(this_y0),
    fit_xrange[0], fit_xrange[1],
  )
  this_fit.SetParName(0, 'C1')
  this_fit.SetParameter(0, 20.) # sqrt(mA)
  this_fit.SetParLimits(0, 0.1, 50.)
  this_fit.SetParName(1, 'C2')
  this_fit.SetParameter(1, 0.250) # MRad
  this_fit.SetParLimits(1, 0.050, 1.000)
  this_fit.SetParName(2, 'C3')
  this_fit.SetParameter(2, 10.) # sqrt(mA)
  this_fit.SetParLimits(2, 0.1, 50.)
  this_fit.SetParName(3, 'C4')
  this_fit.SetParameter(3, 2.) # MRad
  this_fit.SetParLimits(3, 0.01, 10.000)
  this_fit.SetParName(4, 'C5')
  this_fit.SetParameter(4, 10.) # sqrt(mA)
  this_fit.SetParLimits(4, 0.1, 100.)
  print(f'TID fit for "{run_name}":')
  this_graph.Fit(this_fit, "R0")
  # Draw
  this_fit.SetLineWidth(2)
  # this_fit.SetLineColor(colors[iG])
  # this_graph.SetMarkerColor(colors[iG])
  this_graph.SetMarkerSize(0.4)
  # this_graph.SetLineColor(colors[iG])
  this_graph.GetXaxis().SetTitle("Dosage [MRad]")
  this_graph.GetYaxis().SetTitle("Current [mA]")
  this_graph.Draw("AP")
  this_fit.Draw('SAME')
  plot_title = f'Irradiation Run #font[82]{{{run_name}}}'
  AtlasStyle.ATLAS_LABEL(0.2, 0.88, 1, f'Internal')
  AtlasStyle.myText(0.2, 0.84, 1, plot_title)
  AtlasStyle.myText(
    0.45, 0.87, 1.0,
    'y = #left(' +
      ('C_{1}(1-e^{-#phi/C_{2}})-C_{3}(1-e^{-#phi/C_{4}})-C_{5}')
      # ('C_{1}(1-e^{-#phi/C_{2}})-C_{3}(1-e^{-#phi/C_{4}})')
      .replace('-', ' #minus ') +
    '#right)^{2}',
  )
  AtlasStyle.myText(
    0.2, 0.80, 1.0,
    "Params = ({0:.1f}, {1:.3f}, {2:.1f}, {3:.3f}, {4:.1f}), #chi^{{2}}/dof = {5:.1f}"
    # "Params = ({0:.1f}, {1:.3f}, {2:.1f}, {3:.3f}), #chi^{{2}}/dof {4:.1f}"
    .format(
      this_fit.GetParameter(0), this_fit.GetParameter(1),
      this_fit.GetParameter(2), this_fit.GetParameter(3),
      this_fit.GetParameter(4),
      this_fit.GetChisquare() / this_fit.GetNDF(),
    )
  )
  plot_name = f'{run_name}-Fit-IrradTID'
  c1.SaveAs(os.path.join(plot_dir, f'{plot_name}.png'))
  if args.pdf:
    c1.SaveAs(os.path.join(pdf_subdir, f'{plot_name}.pdf'))
  c1.Clear()

  return get_fit_csv_header(this_fit), get_fit_csv_row(this_fit, plot_name)


def plot_DCS_IrradAnnealFit(args, run_name, run_start, data):
  '''Fit after-beam annealing from the irradiation loop DCS data'''
  if run_name not in ('HCC508', 'HCC510TID', 'HCC512TID'):
    return

  # Variable to plot
  var = 'FMC_IDD'

  # Range for fitting
  fit_xrange = (0, 60 * 60 * 12) # s

  # Run data for analysis
  irrad_run_data = IRRAD_RUN_DATA.get(run_name, None)
  beam_start = irrad_run_data['start']
  beam_duration = irrad_run_data['duration']
  beam_end_ts = ((beam_start + beam_duration) - run_start) / dt.timedelta(seconds=1)

  # Make plot (sub)directory
  plot_dir = os.path.join(args.out_dir, 'Fits')
  os.makedirs(plot_dir, exist_ok=True)
  if args.pdf:
    pdf_subdir = os.path.join(plot_dir, 'pdf')
    os.makedirs(pdf_subdir, exist_ok=True)

  # Plot data during the beam
  c1 = ROOT.TCanvas('HCC', 'HCC', 600, 400)
  c1.cd()
  this_graph = ROOT.TGraph()
  this_graph.SetTitle(run_name)
  for ts, val in zip(data['Timestamp'], data[var]):
    s = ts - beam_end_ts
    if s < fit_xrange[0] or s > fit_xrange[1]:
      continue
    this_graph.SetPoint(this_graph.GetN(), s, val)
  # Auto-find y-axis range
  max_yvalue = max(this_graph.GetY())
  min_yvalue = min(this_graph.GetY())
  diff = max_yvalue - min_yvalue
  this_graph.SetMaximum(max_yvalue + diff*0.6)
  this_graph.SetMinimum(min_yvalue - diff*0.2)
  # Fit anneal decay
  # functional form from slide 9 of https://indico.cern.ch/event/728934/#132-slow-tid-irradiations
  this_fit = ROOT.TF1(
    "Fit_Anneal",
    "[0]*e^(-x/[1])+[2]",
    fit_xrange[0], fit_xrange[1],
  )
  this_fit.SetParameter(0, 1) # mA
  this_fit.SetParLimits(0, -100, 100)
  this_fit.SetParName(0, 'C')
  this_fit.SetParameter(1, 1) # s
  this_fit.SetParLimits(1, 1, 5 * 60 * 60 * 24)
  this_fit.SetParName(1, 'tau')
  this_fit.SetParameter(2, min_yvalue) # mA
  this_fit.SetParLimits(2, min_yvalue - 50., min_yvalue + 50.)
  this_fit.SetParName(2, 'D')
  print(f'Anneal fit for "{run_name}":')
  this_graph.Fit(this_fit, "R0")
  # Draw
  this_fit.SetLineWidth(2)
  # this_fit.SetLineColor(colors[iG])
  # this_graph.SetMarkerColor(colors[iG])
  this_graph.SetMarkerSize(0.4)
  # this_graph.SetLineColor(colors[iG])
  this_graph.GetXaxis().SetTitle("Time since end of beam [s]")
  ROOT.TGaxis.SetMaxDigits(4)
  this_graph.GetYaxis().SetTitle("Current [mA]")
  this_graph.Draw("AP")
  this_fit.Draw('SAME')
  plot_title = f'Irradiation Run #font[82]{{{run_name}}}'
  AtlasStyle.ATLAS_LABEL(0.2, 0.88, 1, f'Internal')
  AtlasStyle.myText(0.2, 0.84, 1, plot_title)
  AtlasStyle.myText(
    0.55, 0.87, 1.0,
    'y = C e^{-t/#tau} + D'.replace('-', ' #minus '),
  )
  AtlasStyle.myText(
    0.2, 0.80, 1.0,
    "#tau = {0:.0f} s, C = {1:.3f} mA, D = {2:.1f} mA, #chi^{{2}}/dof = {3:.3f}"
    .format(
      this_fit.GetParameter(1), this_fit.GetParameter(0),
      this_fit.GetParameter(2),
      this_fit.GetChisquare() / this_fit.GetNDF()
    )
  )
  plot_name = f'{run_name}-Fit-IrradAnneal'
  c1.SaveAs(os.path.join(plot_dir, f'{plot_name}.png'))
  if args.pdf:
    c1.SaveAs(os.path.join(pdf_subdir, f'{plot_name}.pdf'))
  c1.Clear()

  return get_fit_csv_header(this_fit), get_fit_csv_row(this_fit, plot_name)


def plot_DCS_IrradDoses(args, run_dict):
  '''Plot DCS data from the irradiation loops wrt dose'''

  # Variables to plot
  vars = (
    'FMC_IDD',
  )

  # Exclude these runs
  exclude_runs = ('HCC506', 'HCC511', 'HCC511TID',)

  # HCC names
  base_run_names = tuple(
    name for name in run_dict
    if len(name) == 6 and name not in exclude_runs
  )

  # Get min and max for plotting
  data_min = {}
  data_max = {}
  for run_name in run_dict:
    if run_name in exclude_runs:
      continue
    irrad_data = run_dict[run_name]['irrad_data']
    for var in vars:
      if not irrad_data[var]:
        continue
      this_min = min(irrad_data[var])
      if var not in data_min:
        data_min[var] = this_min
      else:
        data_min[var] = min(data_min[var], this_min)
      this_max = max(irrad_data[var])
      if var not in data_max:
        data_max[var] = this_max
      else:
        data_max[var] = max(data_max[var], this_max)

  # Color palette
  colors = [
    ROOT.gStyle.GetColorPalette(int(iR * 240 / len(base_run_names)))
    for iR in range(len(base_run_names))
  ]

  # Make plot (sub)directory for combined runs
  plot_dir = os.path.join(args.out_dir, 'Combined')
  os.makedirs(plot_dir, exist_ok=True)
  if args.pdf:
    pdf_subdir = os.path.join(plot_dir, 'pdf')
    os.makedirs(pdf_subdir, exist_ok=True)

  # Setup canvas
  c1 = ROOT.TCanvas('HCC', 'HCC', 600, 400)
  c1.SetLogx()

  # Plot variables as a function of dose for combined runs
  for var in vars:
    if var == 'Timestamp' or var == 'BeamOn':
      continue
    leg = ROOT.TLegend(0.5, 0.8, 0.88, 0.92)
    # leg.SetFillStyle(0)
    leg.SetNColumns((len(base_run_names) // 2) + 1)
    leg.AddEntry(ROOT.nullptr, '#font[82]{#bf{HCC}}', '')
    graphs = []
    first_graph = True
    for iRun, run_name in enumerate(base_run_names):
      run_start = run_dict[run_name]['run_start']
      irrad_data = run_dict[run_name]['irrad_data']

      this_graph = ROOT.TGraph()
      this_graph.SetName(f'graph_{var}_{run_name}')
      this_graph.SetTitle(run_name)
      # Massage data and fill TGraphs
      for dose, beamon, val in zip(
        irrad_data['Dose'], irrad_data['BeamOn'], irrad_data[var],
      ):
        if not beamon:
          continue
        # if var.startswith("AM_"):
        #   # Skip bad AM reads
        #   if var.endswith("_calc") and val < -9998:
        #     continue
        #   elif val == -1:
        #     continue
        # if var.startswith("CAL_") and val < -9998:
        #   # Skip bad CAL reads
        #   continue
        this_graph.SetPoint(this_graph.GetN(), dose, val)
      if run_name + 'TID' in run_dict:
        tid_data = run_dict[run_name + 'TID']['irrad_data']
        for dose, beamon, val in zip(
          tid_data['Dose'], tid_data['BeamOn'], tid_data[var],
        ):
          if not beamon:
            continue
          this_graph.SetPoint(this_graph.GetN(), dose, val)
      # Set y-axis range
      try:
        diff = data_max[var] - data_min[var]
      except KeyError:
        # No data!
        continue
      this_graph.SetMaximum(data_max[var] + diff*0.4)
      this_graph.SetMinimum(data_min[var] - diff*0.2)
      # Set x-axis range
      this_graph.GetXaxis().SetLimits(0.2, 80.)
      # Draw plot
      # this_graph.GetXaxis().SetLabelSize(0.038) # Default 0.04
      this_graph.GetXaxis().SetTitle('Dose [MRad]')
      this_graph.GetYaxis().SetTitle(get_axis_title(var))
      this_graph.SetMarkerSize(0.4)
      this_graph.SetMarkerColor(colors[iRun])
      this_graph.SetLineColor(colors[iRun])
      if first_graph:
        this_graph.Draw("A PL")
        first_graph = False
      else:
        this_graph.Draw("PL SAME")
      leg_str = f'#font[82]{{#bf{{{run_name[3:]}}}}}'
      leg.AddEntry(this_graph, leg_str, 'PL')
      graphs.append(this_graph)
    plot_title = f'Combined runs'
    AtlasStyle.ATLAS_LABEL(0.2, 0.88, 1, f'Internal')
    AtlasStyle.myText(0.2, 0.84, 1, plot_title)
    # AtlasStyle.myText(0.2, 0.80, 1, f'Bandgap setting {bg}')
    leg.Draw()
    # Save plot
    plot_name = f'All-ProbeDoses-{var}'
    c1.SaveAs(os.path.join(plot_dir, f'{plot_name}.png'))
    if args.pdf:
      c1.SaveAs(os.path.join(pdf_subdir, f'{plot_name}.pdf'))
    c1.Clear()


def process_LDOLoop_Probe(probe_file_paths, probe_file_times, run_start):
  '''Gather LDO data from the probe loops for a single run'''
  run_name = file_sort_key(probe_file_paths[0])[0]

  # Run data for analysis
  irrad_run_data = IRRAD_RUN_DATA.get(run_name, None)
  beam_start = irrad_run_data['start']
  beam_duration = irrad_run_data['duration']
  start_dose = irrad_run_data.get('start_dose', 0.)
  doserate = irrad_run_data['doserate']
  run_dose = doserate * (beam_duration / dt.timedelta(hours=1))

  # Variables to get from TTree
  vars = (
    'FMC_bandgap',
    'FMC_VDDraw', #*
    'FMC_VDDreg', #*
    'FMC_IDD', #*
    'FMC_VDD',
    'FMC_NTC0',
    'AM_bandgap',
    'AM_temp',
    'AM_VDDreg',
    'AM_VDDraw',
    'AM_HybridGRD',
    'AM_Cal',
    'AM_bandgap_calc',
    'AM_temp_calc',
    'AM_VDDreg_calc',
    'AM_VDDraw_calc',
    'AM_HybridGRD_calc',
    'AM_Cal_calc',
    'AM_gain',
    'CAL_slope_set',
    'CAL_offset_set',
  )
  calculated_vars = ('Timestamp', 'Dose', 'BeamOn',)
  vars += calculated_vars
  bg_settings = tuple(range(0, 33 + 1))

  # Get data from LDOLoop_tree (probably should use dataframe instead...)
  data = {bg: {var: [] for var in vars} for bg in bg_settings}
  data_min = {}
  data_max = {}
  data_min['Dose'] = data_max['Dose'] = start_dose
  for file_path, probe_file_time in zip(probe_file_paths, probe_file_times):
    file = ROOT.TFile.Open(file_path, "READ")
    tdir_names = [tdir.GetName() for tdir in file.GetListOfKeys()]
    if len(tdir_names) > 1:
      raise ValueError(f"Too many tdir names in {file_path}: {tdir_names}")
    tdir = file.Get(tdir_names[0])
    LDOLoop_tree = tdir.Get('LDOLoop_tree')
    for entry in LDOLoop_tree:
      bg = getattr(entry, 'bandgap_setting')
      for var in vars:
        if var in calculated_vars: continue
        val = getattr(entry, var)
        data[bg][var].append(val)
        # Get min/max
        if var.startswith("AM_"):
          # Skip bad AM reads
          if var.endswith("_calc") and val < -9998:
            continue
          elif val == -1:
            continue
        if var.startswith("CAL_") and val < -9998:
          # Skip bad CAL reads
          continue
        data_min[var] = min(data_min[var], val) if var in data_min else val
        data_max[var] = max(data_max[var], val) if var in data_max else val
      # Timestamp obtained from surrouding irrad loop
      # Then, for ROOT we need to convert to seconds from start of run
      ts = (probe_file_time - run_start) / dt.timedelta(seconds=1)
      data[bg]['Timestamp'].append(ts)
      # Calculate dose as well
      time_in_beam = probe_file_time - beam_start
      dose = start_dose
      beam_on = False
      if time_in_beam > dt.timedelta(0):
        if time_in_beam > beam_duration:
          dose += run_dose
        else:
          dose += doserate * (time_in_beam / dt.timedelta(hours=1))
          beam_on = True
      data[bg]['Dose'].append(dose)
      data_max['Dose'] = dose
      data[bg]['BeamOn'].append(beam_on)

  return data, data_min, data_max


def plot_LDOLoop_Probe(args, run_name, run_start, data, data_min, data_max):
  '''Plot LDO data from the probe loops for a single run'''
  bg_settings = tuple(data)

  # Variables to plot
  vars = (
    'FMC_bandgap',
    'FMC_VDDraw', #*
    'FMC_VDDreg', #*
    'FMC_IDD', #*
    'FMC_VDD',
    'FMC_NTC0',
    'AM_bandgap',
    'AM_temp',
    'AM_VDDreg',
    'AM_VDDraw',
    'AM_HybridGRD',
    'AM_Cal',
    'AM_bandgap_calc',
    'AM_temp_calc',
    'AM_VDDreg_calc',
    'AM_VDDraw_calc',
    'AM_HybridGRD_calc',
    'AM_Cal_calc',
    'AM_gain',
    'CAL_slope_set',
    'CAL_offset_set',
  )

  # Run data for analysis
  irrad_run_data = IRRAD_RUN_DATA.get(run_name, None)
  beam_start = irrad_run_data['start']
  beam_duration = irrad_run_data['duration']
  # start_dose = irrad_run_data.get('start_dose', 0.)
  # doserate = irrad_run_data['doserate']
  # run_dose = doserate * (beam_duration / dt.timedelta(hours=1))

  # Make plot (sub)directory
  plot_dir = os.path.join(args.out_dir, run_name)
  os.makedirs(plot_dir, exist_ok=True)
  if args.pdf:
    pdf_subdir = os.path.join(plot_dir, 'pdf')
    os.makedirs(pdf_subdir, exist_ok=True)

  # Color palette
  colors = [ROOT.gStyle.GetColorPalette(int(iG*240/len(bg_settings))) for iG in range(len(bg_settings))]

  # Plot variables as a function of time
  c1 = ROOT.TCanvas('HCC', 'HCC', 600, 400)
  c1.cd()
  for var in vars:
    if var == 'Timestamp' or var == 'BeamOn':
      continue
    # leg = ROOT.TLegend(0.08, 0.08, 0.92, 0.2)
    leg = ROOT.TLegend(0.55, 0.7, 0.88, 0.92)
    # leg.SetFillStyle(0)
    leg.SetNColumns(6)
    leg.AddEntry(ROOT.nullptr, '#font[82]{#bf{LDO}}', '')
    graphs = []
    first_graph = True
    for bg in data:
      this_graph = ROOT.TGraph()
      this_graph.SetName(f'graph_{var}_{bg}')
      this_graph.SetTitle(run_name)
      # Massage data and fill TGraphs
      for ts, val in zip(data[bg]['Timestamp'], data[bg][var]):
        if var.startswith("AM_"):
          # Skip bad AM reads
          if var.endswith("_calc") and val < -9998:
            continue
          elif val == -1:
            continue
        if var.startswith("CAL_") and val < -9998:
          # Skip bad CAL reads
          continue
        this_graph.SetPoint(this_graph.GetN(), ts, val)
      # Set y-axis range
      try:
        diff = data_max[var] - data_min[var]
      except KeyError:
        # No data!
        continue
      this_graph.SetMaximum(data_max[var] + diff*0.6)
      this_graph.SetMinimum(data_min[var] - diff*0.2)
      # Draw plot
      this_graph.GetXaxis().SetTimeDisplay(1)
      this_graph.GetXaxis().SetTimeOffset(run_start.timestamp())
      this_graph.GetXaxis().SetLabelSize(0.038) # Default 0.04
      if irrad_run_data is not None:
        x_label = f'Time (started {beam_start.strftime(STRPTIME_FMT)})'
      else:
        x_label = 'Time'
      this_graph.GetXaxis().SetTitle(x_label)
      this_graph.GetYaxis().SetTitle(get_axis_title(var))
      this_graph.SetMarkerSize(0.4)
      this_graph.SetMarkerColor(colors[bg])
      this_graph.SetLineColor(colors[bg])
      if first_graph:
        this_graph.Draw("A PL")
        first_graph = False
      else:
        this_graph.Draw("PL SAME")
      leg_str = f'#font[82]{{#bf{{{bg:3}}}}}'
      leg.AddEntry(this_graph, leg_str, 'PL')
      graphs.append(this_graph)
    if irrad_run_data is not None:
      draw_irrad_startstop(c1, run_start, beam_start, beam_duration)
    plot_title = f'Irradiation Run #font[82]{{{run_name}}}'
    AtlasStyle.ATLAS_LABEL(0.2, 0.88, 1, f'Internal')
    AtlasStyle.myText(0.2, 0.84, 1, plot_title)
    # AtlasStyle.myText(0.2, 0.80, 1, f'Bandgap setting {bg}')
    leg.Draw()
    # Save plot
    plot_name = f'{run_name}-Probe-{var}'
    c1.SaveAs(os.path.join(plot_dir, f'{plot_name}.png'))
    if args.pdf:
      c1.SaveAs(os.path.join(pdf_subdir, f'{plot_name}.pdf'))
    c1.Clear()


# def plot_LDO_ProbeDoses(args, run_dict):
#   '''Plot LDO data from the probe loops wrt dose'''
#   for run_name in run_dict:
#     if len(run_name) != 6:
#       continue
#     run_start = run_dict[run_name]['run_start']
#     probe_data = run_dict[run_name]['probe_data']
#     probe_data_min = run_dict[run_name]['probe_data_min']
#     probe_data_max = run_dict[run_name]['probe_data_max']


def plot_LDOLoop_ProbeTIDFit(args, run_name, data):
  '''Fit TID bumps from the irradiation loop DCS data'''
  # For TID fits: censor 5-8 MRad for continuity?
  # TODO: skip for now
  if 'TID' in run_name:
    return

  # Bandgap settings to plot
  bg_settings = tuple(data)

  # Variable to plot
  var = 'FMC_IDD'

  # Range for fitting
  fit_xrange = (0.2, 5.) # MRad

  # Make plot (sub)directory
  plot_dir = os.path.join(args.out_dir, 'Fits')
  os.makedirs(plot_dir, exist_ok=True)
  if args.pdf:
    pdf_subdir = os.path.join(plot_dir, 'pdf')
    os.makedirs(pdf_subdir, exist_ok=True)

  # Color palette
  colors = [ROOT.gStyle.GetColorPalette(int(iG*240/len(bg_settings))) for iG in range(len(bg_settings))]

  # Get y axis range the hard way
  min_yvalue = None
  max_yvalue = None
  for bg in data:
    for dose, beamon, val in zip(data[bg]['Dose'], data[bg]['BeamOn'], data[bg][var]):
      if dose < fit_xrange[0] or dose > fit_xrange[1]:
        continue
      if val < 0:
        # Something's weird with the HCC508 data...
        continue
      min_yvalue = min(min_yvalue, val) if min_yvalue is not None else val
      max_yvalue = max(max_yvalue, val) if max_yvalue is not None else val

  # Plot data during the beam
  c1 = ROOT.TCanvas('HCC', 'HCC', 600, 400)
  c1.cd()
  leg = ROOT.TLegend(0.55, 0.7, 0.88, 0.92)
  leg.SetNColumns(6)
  leg.AddEntry(ROOT.nullptr, '#font[82]{#bf{LDO}}', '')
  graphs = []
  fits = []
  peak_locs = []
  peak_loc_errs = []
  header_row = None
  data_rows = []
  first_graph = True
  for bg in data:
    this_graph = ROOT.TGraph()
    this_graph.SetTitle(f'{run_name}_{bg}')
    this_y0 = None
    for dose, beamon, val in zip(data[bg]['Dose'], data[bg]['BeamOn'], data[bg][var]):
      if not beamon:
        continue
      if this_y0 is None and dose >= fit_xrange[0]:
        this_y0 = val
      this_graph.SetPoint(this_graph.GetN(), dose, val)
    # Set y-axis range
    diff = max_yvalue - min_yvalue
    this_graph.SetMaximum(max_yvalue + diff*0.6)
    this_graph.SetMinimum(min_yvalue - diff*0.2)
    # Fit TID bump
    this_fit = ROOT.TF1(
      "Fit_TIDbump",
      "([0]*(1-e^(-x/[1]))-[2]*(1-e^(-x/[3]))-[4])^2+"+str(this_y0),
      # "([0]*(1-e^(-x/[1]))-[0]*[2]*(1-e^(-x/[3]))-[4])^2+"+str(this_y0),
      fit_xrange[0], fit_xrange[1],
    )
    this_fit.SetParName(0, 'C1')
    this_fit.SetParameter(0, 20.) # sqrt(mA)
    this_fit.SetParLimits(0, 0.1, 50.)
    this_fit.SetParName(1, 'C2')
    this_fit.SetParameter(1, 0.250) # MRad
    this_fit.SetParLimits(1, 0.050, 1.000)
    this_fit.SetParName(2, 'C3')
    this_fit.SetParameter(2, 10.) # sqrt(mA)
    this_fit.SetParLimits(2, 0.1, 50.)
    # this_fit.SetParName(2, 'C3')
    # this_fit.SetParameter(2, 0.5) # 1
    # this_fit.SetParLimits(2, 0., 1.)
    this_fit.SetParName(3, 'C4')
    this_fit.SetParameter(3, 2.) # MRad
    this_fit.SetParLimits(3, 0.01, 10.000)
    this_fit.SetParName(4, 'C5')
    this_fit.SetParameter(4, 10.) # sqrt(mA)
    this_fit.SetParLimits(4, 0.1, 100.)
    print(f'TID fit for "{run_name}-{bg}":')
    # status = this_graph.Fit(this_fit, "R0")
    result = this_graph.Fit(this_fit, "0RS")
    status = result.Status()
    if header_row is None:
      header_row = get_fit_csv_header(this_fit)
      header_row.insert(1, 'bg')
    fit_name = f'{run_name}-Fit-ProbeTID-{bg}'
    data_row = get_fit_csv_row(this_fit, fit_name)
    data_row.insert(1, bg)
    data_rows.append(data_row)
    peak_loc, peak_loc_err = get_TID_peak_loc(this_fit, result)
    peak_locs.append(peak_loc)
    peak_loc_errs.append(peak_loc_err)
    # Draw graph, fit
    this_graph.GetXaxis().SetTitle("Dosage [MRad]")
    this_graph.GetYaxis().SetTitle("Current [mA]")
    this_graph.SetMarkerSize(0.4)
    this_graph.SetMarkerColor(colors[bg])
    this_graph.SetLineColor(colors[bg])
    this_fit.SetLineWidth(1)
    this_fit.SetLineColor(colors[bg])
    if status != 0:
      # fit didn't converge
      this_graph.SetLineStyle(2)
      this_fit.SetLineStyle(2)
    if first_graph:
      this_graph.Draw("AP")
      first_graph = False
    else:
      this_graph.Draw("P SAME")
    this_fit.Draw('SAME')
    leg_str = f'#font[82]{{#bf{{{bg:3}}}}}'
    leg.AddEntry(this_graph, leg_str, 'PL')
    graphs.append(this_graph)
    fits.append(this_fit)
  # Draw decorations
  plot_title = f'Irradiation Run #font[82]{{{run_name}}}'
  AtlasStyle.ATLAS_LABEL(0.2, 0.88, 1, f'Internal')
  AtlasStyle.myText(0.2, 0.84, 1, plot_title)
  # AtlasStyle.myText(
  #   0.45, 0.87, 1.0,
  #   'y = #left(' +
  #     ('C_{1}(1-e^{-#phi/C_{2}})-C_{3}(1-e^{-#phi/C_{4}})-C_{5}')
  #     .replace('-', ' #minus ') +
  #   '#right)^{2}',
  # )
  # AtlasStyle.myText(
  #   0.2, 0.80, 1.0,
  #   "Params = ({0:.1f}, {1:.3f}, {2:.1f}, {3:.3f}, {4:.1f}), #chi^{{2}}/dof {5:.1f}"
  #   .format(
  #     this_fit.GetParameter(0), this_fit.GetParameter(1),
  #     this_fit.GetParameter(2), this_fit.GetParameter(3),
  #     this_fit.GetParameter(4),
  #     this_fit.GetChisquare() / this_fit.GetNDF(),
  #   )
  # )
  avg_peak_loc = np.mean(peak_locs)
  avg_peak_err = np.sqrt(np.sum(np.array(peak_loc_errs)**2)) / len(peak_locs)
  peak_text = f'Peak: {avg_peak_loc:.3f} #pm {avg_peak_err:0.3f} MRad'
  AtlasStyle.myText(0.2, 0.80, 1.0, peak_text)
  leg.Draw()
  # Save plot
  plot_name = f'{run_name}-Fit-ProbeTID'
  c1.SaveAs(os.path.join(plot_dir, f'{plot_name}.png'))
  if args.pdf:
    c1.SaveAs(os.path.join(pdf_subdir, f'{plot_name}.pdf'))
  c1.Clear()

  return header_row, data_rows


def plot_Shmoo_Probe(args, probe_file_paths, probe_file_times, run_start):
  '''Plot Shmoo data from the probe loops'''
  run_name = file_sort_key(probe_file_paths[0])[0]

  # Run data for analysis
  irrad_run_data = IRRAD_RUN_DATA.get(run_name, None)
  beam_start = irrad_run_data['start']
  beam_duration = irrad_run_data['duration']
  start_dose = irrad_run_data.get('start_dose', 0.)
  doserate = irrad_run_data['doserate']
  run_dose = doserate * (beam_duration / dt.timedelta(hours=1))

  # Set up TGraph bins
  BCO_edges = np.array((27, 33, 37, 39, 41, 43, 47, 53))
  LDO_edges = np.array(list(reversed(range(0, 33, 1))))
  VDD_edges = np.array([
    yval / 100 for yval in range(int(0.9*100), int(1.36*100), 2)
  ])
  X_LDO_edges, Y_LDO_edges = np.meshgrid(BCO_edges, LDO_edges, indexing='xy')
  X_VDD_edges, Y_VDD_edges = np.meshgrid(BCO_edges, VDD_edges, indexing='xy')
  X_LDO_centers, Y_LDO_centers = np.meshgrid(
    (BCO_edges[:-1] + BCO_edges[1:]) / 2,
    (LDO_edges[:-1] + LDO_edges[1:]) / 2,
    indexing='xy'
  )
  X_VDD_centers, Y_VDD_centers = np.meshgrid(
    (BCO_edges[:-1] + BCO_edges[1:]) / 2,
    (VDD_edges[:-1] + VDD_edges[1:]) / 2,
    indexing='xy'
  )

  # Gather data
  ldo_name = 'Shmoo_BTC_LDOSetting'
  vdd_name = 'Shmoo_BTC_VDDReg'
  ldo_data = []
  vdd_data = []
  acq_dose = []
  shmoo_status = []
  for file_path, probe_file_time in zip(probe_file_paths, probe_file_times):
    file = ROOT.TFile.Open(file_path, "READ")
    tdir_names = [tdir.GetName() for tdir in file.GetListOfKeys()]
    if len(tdir_names) > 1:
      raise ValueError(f"Too many tdir names in {file_path}: {tdir_names}")
    tdir = file.Get(tdir_names[0])

    ldo_graph = tdir.Get(ldo_name)
    vdd_graph = tdir.Get(vdd_name)
    if not ldo_graph or not vdd_graph:
      # raise ValueError(f'No shmoo in {file_path}!')
      ldo_data.append(None)
      vdd_data.append(None)
      acq_dose.append(None)
      shmoo_status.append(None)
      continue

    shmoo_status.append(shmoo_criteria_check(ldo_graph, vdd_graph))

    for LDO_on_Y in (True, False):
      X = X_LDO_centers if LDO_on_Y else X_VDD_centers
      Y = Y_LDO_centers if LDO_on_Y else Y_VDD_centers
      graph = ldo_graph if LDO_on_Y else vdd_graph
      C = np.zeros_like(X)
      with np.nditer((X, Y, C), op_flags=[['readonly'], ['readonly'], ['writeonly']]) as nditer:
        for x, y, c in nditer:
          if LDO_on_Y:
            # LDO setting stored as negative in TGraph
            z = graph.Interpolate(x, -y)
          else:
            z = graph.Interpolate(x, y)
          if z >= 0.9:
            c[...] = 1
      if LDO_on_Y:
        ldo_data.append(C)
      else:
        vdd_data.append(C)

    time_in_beam = probe_file_time - beam_start
    dose = start_dose
    if time_in_beam > dt.timedelta(0):
      if time_in_beam > beam_duration:
        dose += run_dose
      else:
        dose += doserate * (time_in_beam / dt.timedelta(hours=1))
    acq_dose.append(dose)

  # Remove missing shmoo plots
  probe_file_times = probe_file_times.copy()
  bad_idxs = [i for i, status in enumerate(shmoo_status) if status is None]
  for i in reversed(bad_idxs):
    ldo_data.pop(i)
    vdd_data.pop(i)
    acq_dose.pop(i)
    shmoo_status.pop(i)
    probe_file_times.pop(i)

  # Make plot (sub)directories
  plot_dir = os.path.join(args.out_dir, run_name)
  frame_dir = os.path.join(plot_dir, 'shmoo')
  os.makedirs(plot_dir, exist_ok=True)
  os.makedirs(frame_dir, exist_ok=True)
  if args.pdf:
    pdf_subdir = os.path.join(frame_dir, 'pdf')
    os.makedirs(pdf_subdir, exist_ok=True)

  # Plot data
  cmap = mpc.ListedColormap(['navy', 'red']) # https://stackoverflow.com/a/53361072/10601881
  norm = mpc.BoundaryNorm(np.arange(-0.5,cmap.N), cmap.N)
  foreground_color = (1, 1, 1, 0.7)
  for LDO_on_Y in (True, False):
    X_edges = X_LDO_edges if LDO_on_Y else X_VDD_edges
    Y_edges = Y_LDO_edges if LDO_on_Y else Y_VDD_edges
    C = ldo_data if LDO_on_Y else vdd_data
    fig, ax = plt.subplots(figsize=(5, 3), dpi=96)
    pc = ax.pcolormesh(X_edges, Y_edges, C[0], cmap=cmap, norm=norm)
    cb = plt.colorbar(pc, ticks=[0,1])
    cb.ax.set_yticklabels(['FAIL', 'PASS'], rotation=90)
    ax.set_xlabel('BCO [MHz]')
    ax.set_ylabel('LDO setting' if LDO_on_Y else 'VDDreg [V]')
    if LDO_on_Y: ax.invert_yaxis()
    ax.text(
      0.02, 0.98,
      r'$\bf{ATLAS}$ Internal',
      ha='left', va='top', transform=ax.transAxes,
      bbox=dict(boxstyle='round', fc=foreground_color),
      fontsize=10
    )
    ax.text(
      0.02, 0.02,
      f'Irradiation Run {run_name}',
      ha='left', va='bottom', transform=ax.transAxes,
      bbox=dict(boxstyle='round', fc=foreground_color),
      fontsize=10
    )
    pass_text = ax.text(
      0.98, 0.98,
      f"Shmoo {'PASS' if shmoo_status[0] == 0 else 'ACCEPT'}",
      ha='right', va='top', transform=ax.transAxes,
      bbox=dict(boxstyle='round', fc=foreground_color),
      fontsize=10
    )
    ts_text = ax.text(
      0.98, 0.02,
      f'{probe_file_times[0].strftime(STRPTIME_FMT)}',
      ha='right', va='bottom', transform=ax.transAxes,
      bbox=dict(boxstyle='round', fc=foreground_color),
      fontsize=10
    )
    dose_text = ax.text(
      0.98, 0.12,
      f'{acq_dose[0]:.2f} MRad',
      ha='right', va='bottom', transform=ax.transAxes,
      bbox=dict(boxstyle='round', fc=foreground_color),
      fontsize=10
    )

    def animate(i):
      # ax.pcolormesh(X_edges, Y_edges, C[i], cmap=cmap, norm=norm)
      pc.set_array(C[i])
      shmoo_pass = 'PASS' if shmoo_status[i] == 0 else 'ACCEPT'
      pass_text.set_text(f'Shmoo {shmoo_pass}')
      ts_text.set_text(f'{probe_file_times[i].strftime(STRPTIME_FMT)}')
      dose_text.set_text(f'{acq_dose[i]:.2f} MRad')

    graph_name = 'BTC_LDOSetting' if LDO_on_Y else 'BTC_VDDReg'
    plot_name = f'{run_name}-Probe-Shmoo-{graph_name}'
    def progress_callback(i, _):
      frame_timestamp = probe_file_times[i].strftime('%Y%m%d%H%M%S')
      frame_name = f'{graph_name}-{frame_timestamp}'
      fig.savefig(os.path.join(frame_dir, f'{frame_name}.png'))
      if args.pdf:
        fig.savefig(os.path.join(pdf_subdir, f'{frame_name}.pdf'))

    anim = anm.FuncAnimation(fig, animate, interval=1000, frames=len(C))
    anim.save(os.path.join(plot_dir, f'{plot_name}.mp4'), progress_callback=progress_callback)
    plt.close(fig)


# https://gitlab.cern.ch/atlas-itk-strips-daq/itsdaq-sw/-/blob/PENNProbeHEAD/macros/penn_tests/hcc_tests.cpp#L3577
def shmoo_criteria_check(LDO_tgraph, voltage_tgraph):
  '''This function adapted from process_hccstar_wafer.py:drop_in_shmoo_test_replacement'''
  # Iterate over shmoo parameter space
  BTC_rates = (35, 38, 40, 42)
  # Tuned minimum VDDReg values that HCC must work at for each BCO frequency
  BTC_min_operational_VDDReg = {
    35: 1.12,
    38: 1.12,
    40: 1.12,
    42: 1.14,
  }

  # Value of the test result. 1 is pass, 0 is failure.
  # Becuase we interpolate, use 0.9 to determine success
  value = 0
  status = 0

  this_VDDReg = 0
  for BTC_rate in BTC_rates:
    # Must work at lowest LDO setting (highest achievable VDDReg)
    value = LDO_tgraph.Interpolate(BTC_rate, -6)
    # Sometimes we get a sporadic read failure, check if neighbor works instead
    if value < 0.9:
        value = LDO_tgraph.Interpolate(BTC_rate, -7)
    if value < 0.9:
        status |= 0b10

    # Must work at a tuned lower-edge of the VDDReg range
    this_VDDReg = BTC_min_operational_VDDReg[BTC_rate]
    value = voltage_tgraph.Interpolate(BTC_rate, this_VDDReg)

    # Sometimes we get a sporadic read failure, check if neighbors (roughly +- 10mV) both work
    if value < 0.9:
      value = min(
        voltage_tgraph.Interpolate(BTC_rate, this_VDDReg + 0.01),
        voltage_tgraph.Interpolate(BTC_rate, this_VDDReg - 0.01)
      )
    if value < 0.9:
      status |= 0b100

  return status

# BEGIN transclude from process_hccstar_wafer.py
class Quality(enum.IntFlag):
    '''Chip qualities for the database'''
    BAD              = 0b0
    GOOD             = 0b1
    ACCEPT_AMFMC     = 0b10
    ACCEPT_DCSRANGES = 0b100
    ACCEPT_SHMOO     = 0b1000
    ACCEPT_ANALOG    = 0b10000
    ACCEPT_HCCID     = 0b100000
    ACCEPT_L1R3      = 0b1000000
    ACCEPT_MISSING   = 0b10000000

MIN_ACCEPT = 0b10 # Smallest ACCEPT level


VERBOSE = 1


def find_test_errors(file_name, dir_name) -> list[tuple[Quality, str, str]]:
    '''Get list of ERROR and ACCEPT tests for the given directory'''
    # Need to special-case tests by wafer
    wafer_name = dir_name.split('_')[0]
    short_test = wafer_name in ("VYD96VH",)
    # Loop over tests in file and pick out ERROR/FAILURE and ACCEPT tests
    errors: list[tuple[int, str, str]] = []
    this_file = ROOT.TFile.Open(file_name, "READ")
    this_dir = this_file.Get(dir_name)
    key_names = {
        key.GetName() for key in this_dir.GetListOfKeys()
        # if type(key.ReadObj()) == ROOT.TNamed
        if key.GetClassName() == 'TNamed'
    }
    for key_name in key_names:
        status = this_dir.Get(key_name)
        if not status:
            # Data corruption
            # (Special case 1st wafer because hadn't implemented this test yet)
            if wafer_name not in ("VYD96VH",):
                errors.append((Quality.ACCEPT_MISSING, key_name, "CORRUPT: Can't Get"))
            else:
                print(tc.YELLOW + f"WARNING: skipping corrupt key {key_name} for first wafer (Can't Get)" + tc.RESET)
            continue
        try:
            result = status.GetTitle()
        except TypeError:
            # Data corruption from printing test
            # errors.append((-1, key.GetName(), "Can't GetTitle"))
            errors.append((Quality.ACCEPT_MISSING, key_name, "CORRUPT: Can't GetTitle"))
            continue
        try:
            test = status.GetName()
        except TypeError:
            # Data corruption
            # errors.append((-1, key.GetName(), "Can't GetName"))
            errors.append((Quality.ACCEPT_MISSING, key_name, "CORRUPT: Can't GetName"))
            # Don't continue, just in case Title is readable
            test = key_name
        else:
            if test != key_name:
                # Data corruption again
                # (Special case 1st wafer because hadn't implemented this test yet)
                # errors.append((-1, key.GetName(), f"key doesn't match GetName: {test!r}"))
                if wafer_name not in ("VYD96VH",):
                    errors.append((Quality.ACCEPT_MISSING, key_name, f"CORRUPT: key doesn't match GetName: {test!r}"))
                else:
                    print(tc.YELLOW + f"WARNING: skipping corrupt key {key_name} for first wafer (doesn't match GetName: {test!r})" + tc.RESET)
                test = key_name
        # Startup tests ignore the namecycle
        if test.startswith('Probe_Startup'):
            if test == 'Probe_Startup' and 'Probe_Startup_2' in key_names:
                continue
            elif test == 'Probe_Startup_2' and 'Probe_Startup_3' in key_names:
                continue
        if 'ACCEPT' in result:
            # Some Titles contain ERROR but end with ACCEPT status
            # TODO: at some point ITSDAQ will emit these levels and we can trust them?
            if 'DCSRanges' in test:
                level = Quality.ACCEPT_DCSRANGES
            elif 'ShmooBTCVDDReg' in test or 'SchmooBTCVDDReg' in test:
                # level = Quality.ACCEPT_SHMOO
                if VERBOSE > 1:
                    print(tc.YELLOW + f'WARNING: skipping {test} result {result!r} for drop-in' + tc.RESET)
                continue # We use drop-in Shmoo test for this
            elif 'LDOLoop' in test:
                if 'Had a bad AM read' in result:
                    # We should still report bad AM reads as ACCEPT!
                    level = Quality.ACCEPT_ANALOG
                else:
                    # We use drop-in analog test for voltage range
                    if VERBOSE > 1:
                        print(tc.YELLOW + f'WARNING: skipping {test} result {result!r} for drop-in' + tc.RESET)
                    continue
            elif 'WrongHCCID' in test:
                level = Quality.ACCEPT_HCCID
            else:
                # If you got here, add the test criteria!
                raise ValueError(f'Unknown ACCEPT test {test} for {file_name}: {result}')
            errors.append((level, test, result))
        elif 'ERROR' in result or 'FAILURE' in result:
            if test.startswith('Triplicated') and len(test.split('_')) > 2:
                # Hack to deal with output suppression of Triplicated Clocks test
                # TODO: iterate over previous cycles instead to check for pass
                continue
            result = status.GetTitle()
            errors.append((Quality.BAD, test, result))
    # Drop-in tests
    error = drop_in_analog_test_replacement(this_dir, wafer_name)
    if error:
        errors.append(error)
    if not short_test:
        error = drop_in_shmoo_test_replacement(file_name, this_dir)
        # error = drop_in_shmoo_test_replacement_alt(file_name, this_dir)
        if error:
            errors.append(error)
    this_file.Close()
    return errors


def drop_in_analog_test_replacement(this_dir, wafer_name):
    '''Drop-in replacement of analog LDO loop test'''
    error = None
    # Get LDO tree
    LDO_tree = this_dir.Get("LDOLoop_tree")
    if not LDO_tree:
        error = (Quality.ACCEPT_MISSING, "LDOLoop_tree", "CORRUPT: can't Get")
        return error
    VDDreg_maxsetting = []
    VDDreg_minsetting = []
    # Get max regulated voltage by considering only first bandgap setting
    min_LDO_setting = 33 if wafer_name not in ("VYD96VH",) else 16
    for ev in LDO_tree:
        if ev.bandgap_setting == 0:
            VDDreg_maxsetting.append(ev.FMC_VDDreg)
        elif ev.bandgap_setting == min_LDO_setting:
            VDDreg_minsetting.append(ev.FMC_VDDreg)
    # VDDreg measurements have precision of 0.005
    # Test fails if sample average outside required regulated voltage
    max_VDDreg = round(sum(VDDreg_maxsetting) / len(VDDreg_maxsetting), 4)
    min_VDDreg = round(sum(VDDreg_minsetting) / len(VDDreg_minsetting), 4)
    if max_VDDreg < 1.28:
        level = Quality.ACCEPT_ANALOG
        test = "LDOLoop"
        result = tc.RED + f"LDOLoop: ACCEPT ({level}): drop-in {max_VDDreg=}" + tc.RESET
        error = (level, test, result)
    if min_VDDreg > 1.12 and wafer_name not in ("VYD96VH",):
        level = Quality.ACCEPT_ANALOG
        test = "LDOLoop"
        result = tc.RED + f"LDOLoop: ACCEPT ({level}): drop-in {min_VDDreg=}" + tc.RESET
        error = (level, test, result)
    return error


# https://gitlab.cern.ch/atlas-itk-strips-daq/itsdaq-sw/-/blob/PENNProbeHEAD/macros/penn_tests/hcc_tests.cpp#L3577
def drop_in_shmoo_test_replacement(file_name, this_dir):
    '''Drop-in replacement of Shmoo test.
    Logic mostly copied from hcc_tests.cpp/shmoo_criteria_check
    '''
    error = None
    test = 'ShmooBTCVDDReg'
    # Get LDOSetting and VDDReg TGraph2D's
    if 'V0D97AH' in file_name:
        # Early wafers have an 'sch'
        ldo_name = 'Schmoo_BTC_LDOSetting'
        vdd_name = 'Schmoo_BTC_VDDReg'
    else:
        ldo_name = 'Shmoo_BTC_LDOSetting'
        vdd_name = 'Shmoo_BTC_VDDReg'
    LDO_tgraph = this_dir.Get(ldo_name)
    if not LDO_tgraph:
        level = Quality.ACCEPT_MISSING
        result = tc.RED + f"{test}: ACCEPT ({level}): Couldn't Get LDOSetting graph" + tc.RESET
        error = (level, test, result)
        return error
    voltage_tgraph = this_dir.Get(vdd_name)
    if not voltage_tgraph:
        level = Quality.ACCEPT_MISSING
        result = tc.RED + f"{test}: ACCEPT ({level}): Couldn't Get VDDReg graph" + tc.RESET
        error = (Quality.ACCEPT_MISSING, test, result)
        return error
    # Iterate over shmoo parameter space
    # BTC_rates = (35, 38, 40, 42, 45)
    BTC_rates = (35, 38, 40, 42)
    # Tuned minimum VDDReg values that HCC must work at for each BCO frequency
    BTC_min_operational_VDDReg = {
        35: 1.12,
        38: 1.12,
        40: 1.12,
        42: 1.14,
        # 45: 1.16,
    }

    # Value of the test result.  1 is pass, 0 is failure.
    # Becuase we interpolate, use 0.9 to determine success
    value = 0
    status = 0

    this_VDDReg = 0
    for BTC_rate in BTC_rates:
        # Must work at lowest LDO setting (highest achievable VDDReg)
        value = LDO_tgraph.Interpolate(BTC_rate, -6)
        # Sometimes we get a sporadic read failure, check if neighbor works instead
        if value < 0.9:
            value = LDO_tgraph.Interpolate(BTC_rate, -7)
        if value < 0.9 :
            status |= 0b10

        # Must work at a tuned lower-edge of the VDDReg range
        this_VDDReg = BTC_min_operational_VDDReg[BTC_rate]
        value = voltage_tgraph.Interpolate(BTC_rate, this_VDDReg)

        # Sometimes we get a sporadic read failure, check if neighbors (roughly +- 10mV) both work
        if value < 0.9:
            value = min(
                    voltage_tgraph.Interpolate(BTC_rate, this_VDDReg + 0.01),
                    voltage_tgraph.Interpolate(BTC_rate, this_VDDReg - 0.01)
                    )
        if value < 0.9:
            status |= 0b100

    if status != 0:
        level = Quality.ACCEPT_SHMOO
        result = tc.RED + f'{test}: ACCEPT ({level}) with status {status}' + tc.RESET
        error = (level, test, result)
    return error


# END transclude from process_hccstar_wafer.py


def process_errors_Probe(probe_file_paths, probe_file_times, run_start):
  '''TODO'''
  run_name = file_sort_key(probe_file_paths[0])[0]

  # Run data for analysis
  irrad_run_data = IRRAD_RUN_DATA.get(run_name, None)
  beam_start = irrad_run_data['start']
  beam_duration = irrad_run_data['duration']
  start_dose = irrad_run_data.get('start_dose', 0.)
  doserate = irrad_run_data['doserate']
  run_dose = doserate * (beam_duration / dt.timedelta(hours=1))

  data = {}


if __name__ == '__main__':
  '''Parse arguments'''
  parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument("--in-dir", dest='in_dir', default='', help="Input directory containing ROOT files from radiation tests of wafers.")
  parser.add_argument("--out-dir", dest='out_dir', default='', help="Output directory containing plots.")
  parser.add_argument("--purge", action='store_true', help="Don't use cached data")
  parser.add_argument("--batch", "-b", action='store_true', default=False, help="Run ROOT in batch mode")
  parser.add_argument("--pdf", action='store_true', default=False, help="Make pdf figures")
  # parser.add_argument("--eps", action='store_true', default=False, help="Make eps figures")
  args = parser.parse_args()
  args = parser.parse_args([
    '--in-dir', 'irrad_data',
    '--out-dir', 'irrad_plots',
    '--purge',
    '--batch',
  ]) # DEBUG
  ROOT.gROOT.SetBatch(args.batch)
  ROOT.gErrorIgnoreLevel = ROOT.kInfo + 1
  # ROOT.Math.MinimizerOptions.PrintDefault('Migrad')
  ROOT.Math.MinimizerOptions.SetDefaultMaxFunctionCalls(10_000)
  ROOT.Math.MinimizerOptions.SetDefaultMaxIterations(10_000)
  
  main(args)
